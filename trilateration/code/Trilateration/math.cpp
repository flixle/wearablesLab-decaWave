#include "math.h"
#include <cmath>
#include "Trilateration.h"
#include <fstream>
#include <iostream>
#include <cstdlib>

float sq (float value)
// Return the square of the value
{
return (value*value);
};

float determinant_2(float a1,float a2,float b1,float b2)
/*
Compute the determinant of a 2x2 matrix [a1 b1; a2 b2]
*/
{
    return (a1*b2 - a2*b1);
}

float determinant_3(float a,float b,float c,float d,float e,float f,float g,float h,float i)
/*
Compute the determinant of a 3x3 matrix [a b c; d e f; g h i]
*/
{
return ((a*e*i+d*h*c+g*b*f)-(g*e*c+a*h*f+d*b*i));
}

void init_ref(struct Data * data){

    float x,h,d,b,c,e;
    std::cout << "Enter distance between the first and second anchor: ";
    std::cin >> x;
    std::cout << "Enter distance between the first and third anchor: ";
    std::cin >> d;
    std::cout << "Enter distance between the second and third anchor: ";
    std::cin >> h;
    std::cout << "Enter distance between the first and forth anchor: ";
    std::cin >> b;
    std::cout << "Enter distance between the second and forth anchor: ";
    std::cin >> e;
    std::cout << "Enter distance between the third and forth anchor: ";
    std::cin >> c;

    data->pos_1[0] = 0;
    data->pos_1[1] = 0;
    data->pos_1[2] = 0;
    data->pos_2[0] = x;
    data->pos_2[1] = 0;
    data->pos_2[2] = 0;
    data->pos_3[0] = (sq(h)-sq(d)-sq(x))/(-2*x);
    data->pos_3[1] = sqrt(abs(sq(d)-sq(data->pos_3[0])));
    data->pos_3[2] = 0;
    data->pos_4[0] = (sq(b)+sq(x)-sq(e))/(2*x);
    data->pos_4[1] = (sq(b)-sq(c)+ sq(data->pos_3[0]) + sq(data->pos_3[1]) - 2*data->pos_3[0]*data->pos_4[0])/(2*data->pos_3[1]);
    data->pos_4[2] = sqrt(abs(sq(b) - sq(data->pos_4[0]) - sq(data->pos_4[1])));
};
