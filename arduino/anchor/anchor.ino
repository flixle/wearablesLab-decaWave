
#include <SPI.h>j
#include "DW1000Ranging.h"
#include "DW1000Device.h"

int led = 3;
int interval = 5000;
int lastTime = 0;
int currentTime = 0;
int low = 0;

byte shortAdr[2] = {0x2d, 0xf1};
char* eui = "82:17:5B:D5:A9:9A:E2:93";

void setup() {
  Serial.begin(57600);
  delay(1000);
  pinMode(led, OUTPUT);
  digitalWrite(led, HIGH);
  //init the configuration
  DW1000Ranging.initCommunication(9, 10); //Reset and CS pin
  //define the sketch as anchor. It will be great to dynamically change the type of module
  DW1000Ranging.attachNewRange(newRange);
  //we start the module as an anchor
  DW1000Ranging.startAsAnchor(eui, DW1000.MODE_LONGDATA_RANGE_ACCURACY,shortAdr);
  //byte sAdr[8]= {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  byte *sAdr = DW1000Ranging.getCurrentShortAddress();
  Serial.print("short: ");
  for (int i = 1; i >= 0; i--) {
    Serial.print(sAdr[i],HEX);
    
  }
  Serial.println("");
  Serial.println("##############");
}


void loop() {
  DW1000Ranging.loop();
  currentTime = millis();
  if (currentTime - lastTime > interval) {
    lastTime = currentTime;
    if (low == 1) {
      low = 0;
      digitalWrite(led, HIGH);
      interval = 10000;
    } else {
      digitalWrite(led, LOW);
      low = 1;
      interval = 100;
    }
  }
}

void newRange() {
  Serial.print("from: "); Serial.print(DW1000Ranging.getDistantDevice()->getShortAddress(), HEX);
  Serial.print("\t Range: "); Serial.print(DW1000Ranging.getDistantDevice()->getRange()); Serial.print(" m");
  Serial.print("\t RX power: "); Serial.print(DW1000Ranging.getDistantDevice()->getRXPower()); Serial.println(" dBm");

}


