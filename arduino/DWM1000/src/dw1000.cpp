#include <SPI.h>
#include <dw1000.h>

// __________________________________________________________________________
void readWriteToRegister(boolean readWrite, byte regAddr, 
    unsigned short octets, byte* data, unsigned short sub, 
    unsigned short extAddr ){
  byte header [3];
  int  headerLen = 1;
  int  i;
   
  if (readWrite){
    header[0] = 0x80; //Set MSB high = write operation
    } else {
      header[0] = 0x00; //Set MSB low = read operation
      }
  
  if(sub == 0) {
    header[0] = header[0] | (byte)regAddr;
  } else {
      header[0] = header[0] | 0x40 | (byte)(regAddr);
      
      if(sub == 1) { // We need to use sub address
        
        header[1] = (byte)(extAddr >> 8);
        headerLen = 2;
        
      } else { // We do need to use extended address
              
          header[1] = 0x80 | (byte)(extAddr);
          header[2] =  (byte)((extAddr >> 7));
          headerLen = 3;
        
      }
  }

  SPI.beginTransaction(SPISettings(16000000L, MSBFIRST, SPI_MODE0));
  digitalWrite(chipSelect, LOW);

  for(i = 0; i < headerLen; i++) {
    SPI.transfer(header[i]);
    //Serial.println(header[i],BIN);
  }

  if(readWrite){
    
    for(i = 0; i < octets; i++) {
      SPI.transfer(data[i]);
    }
    
  } else {
    
      for(i = 0; i < octets; i++) {
        data[i] = SPI.transfer(0x00);
      }
  
    }
  delayMicroseconds(5);
  digitalWrite(chipSelect, HIGH);
  SPI.endTransaction();
  return;
}

// __________________________________________________________________________
void initConfig() {
  byte tmp[8] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

  // AGC_TUNE1
  tmp[1] = 0x88;
  tmp[0] = 0x70;
  readWriteToRegister(1, 0x23, 2, tmp, 1, 0x0400 );
  // AGC_TUNE2
  tmp[3] = 0x25;
  tmp[2] = 0x02;
  tmp[1] = 0xA9;
  tmp[0] = 0x07;
  readWriteToRegister(1, 0x23, 4, tmp, 1, 0x0c00 );

  // DRX_TUNE2
  tmp[3] = 0x31;
  tmp[2] = 0x1A;
  tmp[1] = 0x00;
  tmp[0] = 0x2D;
  readWriteToRegister(1, 0x27, 4, tmp, 1, 0x0800 );
  // NTM
  tmp[0] = 0x0D;
  readWriteToRegister(1, 0x2e, 1, tmp, 2, 0x0806 );
  // LDE_CFG2
  tmp[1] = 0x16;
  tmp[0] = 0x07;
  readWriteToRegister(1, 0x2e, 2, tmp, 1, 0x1806 );
  // TX_POWER
  tmp[3] = 0x1e;
  tmp[2] = 0x08;
  tmp[1] = 0x02;
  tmp[0] = 0x22;
  readWriteToRegister(1, TX_POWER, 4, tmp, 0 );
  // RF_TXCTRL
  tmp[3] = 0x00;
  tmp[2] = 0x1e;
  tmp[1] = 0x3f;
  tmp[0] = 0xe0;
  readWriteToRegister(1, 0x28, 4, tmp, 1, 0x0c00 );
  // TC_PGDELAY
   tmp[0] = 0xc0;
  readWriteToRegister(1, 0xc0, 1, tmp, 1, 0x0b00 );
  // FS_PLLTUNE
  tmp[0] = 0xbe;
  readWriteToRegister(1, 0x2b, 1, tmp, 1, 0x0b00 );
  // LDELOAD
  //  PMSC_CTRL0
  tmp[1] = 0x03;
  tmp[0] = 0x01;
  readWriteToRegister(1, 0x36, 2, tmp, 1, 0x0000 );

  tmp[1] = 0x80;
  tmp[0] = 0x00;

  // writeToRegister(0x36, 0x06, tmp, 2);
  readWriteToRegister(1, 0x36, 2, tmp, 1, 0x0600 );
  delayMicroseconds(150);
  tmp[1] = 0x02;
  tmp[0] = 0x00;
  readWriteToRegister(1, 0x36, 2, tmp, 1, 0x0000 );
  return;
}
