#include <fstream>
#include <sstream>
#include <iostream>
#include "Trilateration.h"
#include <iostream>
#include <windows.h>
#include "file.h"

#include <conio.h>
using namespace std;

void save_data(ofstream& doc, struct Data data, float result[3])
{
        if(!doc)
        {
            cout << "Error: file cannot be open" << endl;
        }
        else
        {
            /* We save :
    x_target y_target z_target anchor_number x_anchor y_anchor distance_from_anchor */

            doc <<result[0]<<" "<<result[1]<<" "<<result[2]
            <<" 1 "<<data.pos_1[0]<<" "<<data.pos_1[1]<<" "<<data.pos_1[2]<<" "<<data.dis_1
            <<" 2 "<<data.pos_2[0]<<" "<<data.pos_2[1]<<" "<<data.pos_2[2]<<" "<<data.dis_2
            <<" 3 "<<data.pos_3[0]<<" "<<data.pos_3[1]<<" "<<data.pos_3[2]<<" "<<data.dis_3
            <<" 4 "<<data.pos_4[0]<<" "<<data.pos_4[1]<<" "<<data.pos_4[2]<<" "<<data.dis_4<<endl;
        }
};

struct Data import_data(string name, int line)
{
    struct Data st_result;
    std::string read_line;
    std::stringstream f_coordinate;
    std::stringstream f_distance;
    ifstream fichier(name.c_str()); //Open the file in reading mode
    int index_line = 0;

    if(!fichier)
    {
        cout << "Error: file cannot be open" << endl;
    }
    else
    {
        while (index_line <= line)
        {
            std::getline(fichier, read_line);
            //Get the anchors coordinates
            if (index_line==0) // First line contains the anchors coordinates
            {
                f_coordinate.str(read_line);
                f_coordinate >> st_result.pos_1[0] >> st_result.pos_1[1] >> st_result.pos_1[2]
                >> st_result.pos_2[0] >> st_result.pos_2[1] >> st_result.pos_2[2]
                >> st_result.pos_3[0] >> st_result.pos_3[1] >> st_result.pos_3[2]
                >> st_result.pos_4[0] >> st_result.pos_4[1] >> st_result.pos_4[2];

                if (line==0)
                {
                    cout << "Only first line read, no values for the distances" << endl;
                    // Initialize the distance to avoid having Nan in the saving doc which would crash the graphic interface
                    st_result.dis_1 = 0;
                    st_result.dis_2 = 0;
                    st_result.dis_3 = 0;
                    st_result.dis_4 = 0;
                    return(st_result);
                }
            }
            index_line ++;
        }
        f_distance.str(read_line);
        f_distance >> st_result.dis_1 >> st_result.dis_2 >> st_result.dis_3 >> st_result.dis_4;
    }
    fichier.close();
    return(st_result);
};

/*  Print of the imported datas
    cout << testData.pos_1[0] <<" "<< testData.pos_1[1] <<" "<< testData.pos_1[2] <<" "<<testData.dis_1 << endl;
    cout << testData.pos_2[0] <<" "<< testData.pos_2[1] <<" "<< testData.pos_2[2] <<" "<<testData.dis_2 << endl;
    cout << testData.pos_3[0] <<" "<< testData.pos_3[1] <<" "<< testData.pos_3[2] <<" "<<testData.dis_3 << endl;
*/

HANDLE init_port(LPCSTR port){

    HANDLE m_hComPort = CreateFile(port, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0,NULL);
    return m_hComPort;
};

void close_com(HANDLE m_hComPort){
     CloseHandle(m_hComPort);
};

bool read(struct Data * data, HANDLE m_hComPort){

    //COMMTIMEOUTS timeouts;
    char input;
    string distance;
    DWORD dwReading;
    bool test;
    int dwSize = 1; // number of read bytes (8bits)
    char* pEnd;

    //cout <<"before test" <<endl;
    if (m_hComPort == INVALID_HANDLE_VALUE)
    {
        //  Handle the error.
        printf ("CreateFile failed with error %d.\n", GetLastError());
        return (1);
    }

    int i = 0;
    // Read the input of the bluetooth module character by character and concatenate them in a string
    do {
       // cout << "read";
        // set short timeouts on the comm port.
        /*timeouts.ReadIntervalTimeout = 5;
        timeouts.ReadTotalTimeoutMultiplier = 1;
        timeouts.ReadTotalTimeoutConstant = 1;
        timeouts.WriteTotalTimeoutMultiplier = 1;
        timeouts.WriteTotalTimeoutConstant = 1;
        if (!SetCommTimeouts(m_hComPort , &timeouts)){
            cout << "\n Time out Error"<<endl;
            break;
            }
        else{*/
            test = ReadFile(m_hComPort, &input,  dwSize, &dwReading, NULL);
            if (test && (dwReading!=0) && (input =='0'|input =='1'|input =='2'|input =='3'|input =='4'|input =='5'|input =='6'|input =='7'|input =='8'|input =='9'|input ==' '|input =='.'|input =='\n')){
                distance = distance + input;
                //cout << "readdone " << input <<" a " << endl;
                }

    }while (input != '\n');

    //cout << distance <<endl;
    // Divided the string message in float
    data->dis_1 = strtof(&distance[0], &pEnd) *100;
    data->dis_2 = strtof(pEnd, &pEnd) *100;
    data->dis_3 = strtof(pEnd, &pEnd) *100;
    data->dis_4 = strtof(pEnd, NULL) *100;
    //test = PurgeComm(m_hComPort, PURGE_RXCLEAR |PURGE_RXABORT |PURGE_TXABORT | PURGE_TXCLEAR);
    //cout << "convertion over" << endl;
    return true;
    };

    bool send_mess(HANDLE m_hComPort){

    int wake_up[] = {0,0,0,0,1,1,1,1}; //Wake up message 00001111
    DWORD dwSending;

        if(m_hComPort == INVALID_HANDLE_VALUE)
        return false;

        WriteFile(m_hComPort, &wake_up[0], sizeof(wake_up), &dwSending, NULL);

return true;
};

