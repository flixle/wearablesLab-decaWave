close all;
clear all;

% Only use values manually entrer in the matrix bellow. 
%% Target
x_t = [307.46 236.62 176.680 151.25 169.41];
y_t = [353.06 304.02 245.89 171.42 126.01];
i = [1 2 3 4 5];
t =[1 1 0];

%% RSSI-Based 
x_rssi = [307.4543 236.611987 176.675094 151.245632 169.406866];
y_rssi = [353.0547 304.014990 245.888579 171.420088 126.007104];

%% Least square 
x_least = [275.5601 243.907097 221.15516 223.563141 245.718949];
y_least = [ 280.1856 320.682215 347.512737 336.644768 300.358235];

%% Plot
figure;
subplot(1,2,1);
plot(i,x_t,'bx',i,x_least,'ko',i,x_rssi,'ro');
legend('target','Least square','RSSI-based');
xlabel('point number');
ylabel('x value');
subplot(1,2,2);
plot(i,y_t,'bx',i,y_least,'ko',i,y_rssi,'ro');
xlabel('point number');
ylabel('y value');