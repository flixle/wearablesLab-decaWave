clear all;
close all;

%% RSSI-Based or the least saquare algorithm which one is the more precise ?
% We compute both of them with the same data and compare the error.
%%---------------------------------------------------------------------%%
%% Data
%Position of the anchor
% P=[x1 x2 ...
%    y1 y2 ...]

P=[705.26 56.79 198.48
   176.87 380.31 46.08];
n = ndims(P); 
N = length(P);

%distance btw the receiver and the anchor
R=[538.26 
   278.12 
   85.04];
 
target = [169.41 126.01];

%% Computation of the least square's algorithm 
% See documentation for more informations
% a,B and c
a=0;
B=0;
c=0;

for i = 1:N
    a = a + P(:,i)*P(:,i)'*P(:,i) - R(i)*R(i)*P(:,i);
    B = B +(-2 * P(:,i) * P(:,i)' - (P(:,i)'*P(:,i))*eye(n))+ R(i)*R(i)*eye(n);
    c = c + P(:,i);
end 
a = a./N;
B = B./N;
c = c./N;

D = B + 2*c*c' + (c'*c)*eye(n);
f = a + B*c + 2*c*(c')*c;

H=0;
for i = 1:N
    H = H + P(:,i)*P(:,i)'+2*c*c';
end
H = -2*H/N;

for i = 1:n
    H_prime(i) = H(i) - H(n);
    f_prime(i) = f(i) - f(n);
end

H_prime = H_prime';
f_prime = f_prime';

q = - inv(H) * f;
p_0 = q + c;
Q = 0;
for i = 1:N
    Q = Q + R(i)*R(i)- P(:,i)'*P(:,i);
end
Q = Q/N + c'*c;
m=q'*q;

%% RSSI-based algorithm 
% See documentation for more informations

alpha = (R(1)^2-R(2)^2)-(P(1,1)^2-P(1,2)^2)-(P(2,1)^2-P(2,2)^2);
beta = (R(1)^2-R(3)^2)-(P(1,1)^2-P(1,3)^2)-(P(2,1)^2-P(2,3)^2);

X_num = [alpha 2*(P(2,2)-P(2,1))
         beta  2*(P(2,3)-P(2,1))];
     
Y_num = [2*(P(1,2)-P(1,1)) alpha
         2*(P(1,3)-P(1,1)) beta];
     
den = [2*(P(1,2)-P(1,1)) 2*(P(2,2)-P(2,1))
         2*(P(1,3)-P(1,1)) 2*(P(2,3)-P(2,1))];
     
X = det(X_num)/det(den);
Y = det(Y_num)/det(den);

%% Comparison RSSI-based and least square
%print the final values of x and y and the error pourcentage 
fprintf('\nTarget value X=%f and Y=%f\n',target(1),target(2));
fprintf('Least square: X=%f Y=%f; Error: err_x: %f err_y: %f\n',p_0(1,1),p_0(2,1),abs((p_0(1,1)-target(1))/target(1)*100),abs((p_0(2,1)-target(2))/target(2)*100));
fprintf('COLA: X=%f Y=%f; Error: err_x: %f err_y: %f \n',X,Y,abs((X-target(1))/target(1)*100),abs((Y-target(2))/target(2)*100));

%% The RSSI-based algorithm is more efficient taking in account precision. Moreover
% the complexity is low only a few additions and multiplications.