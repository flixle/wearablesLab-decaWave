
#include <SPI.h>
#include "DW1000Ranging.h"
#include "DW1000Device.h"

#define windowSize 5

// Calibration
// Real:Measured Distance
float cal[4] = {
      7.71/9.04,
      7.71/8.83,
      7.71/8.81,
      7.71/4.88

    // 1,1,1,1
    
};


int led = 6;
int interval = 5000;
int lastTime = 0;
int currentTime = 0;
int low = 0;

unsigned int sAdr [4] = {0xF12A, 0xF12B, 0xF12C, 0xF12D};
float recDist[4][windowSize] = {{0.0, 0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0, 0.0}};
float sumDist[4] = {0.0, 0.0, 0.0, 0.0};
// float latestDist[4] = {0.0, 0.0, 0.0, 0.0};
int index[4] = {0, 0, 0, 0};
int count = 0;


void setup() {
  Serial.begin(57600);
  delay(1000);
  pinMode(led, OUTPUT);
  digitalWrite(led, HIGH);
  //init the configuration
  DW1000Ranging.initCommunication(9, 10); //Reset and CS pin
  //define the sketch as anchor. It will be great to dynamically change the type of module
  DW1000Ranging.attachNewRange(newRange);
  //we start the module as a tag
  DW1000Ranging.startAsTag("7D:00:22:EA:82:60:3B:9C", DW1000.MODE_LONGDATA_RANGE_ACCURACY);

}

void loop() {
  DW1000Ranging.loop();
  if (count >= 4) {
    for (int i = 0; i < 4; i++) {
      // Serial.print(index[i]);
      // Serial.print(":");

      float tmp = sumDist[i] / windowSize * cal[i];
      Serial.print(tmp);
      if (i < 3)
        Serial.print(" ");
    }
    Serial.println();
    count = 0;
  }

  currentTime = millis();
  if (currentTime - lastTime > interval) {
    lastTime = currentTime;
    if (low == 1) {
      low = 0;
      digitalWrite(led, HIGH);
      interval = 1000;
    } else {
      digitalWrite(led, LOW);
      low = 1;
      interval = 100;
    }
  }
}

void newRange() {
  // Serial.print("from: "); Serial.print(DW1000Ranging.getDistantDevice()->getShortAddress(), HEX);
  unsigned int shortTmp = DW1000Ranging.getDistantDevice()->getShortAddress();
  int i = 0;
  while (sAdr[i] != shortTmp && i < 4) {
    i++;
  }
  count++;
  // latestDist[i] = DW1000Ranging.getDistantDevice()->getRange();
  recDist[i][index[i]] = DW1000Ranging.getDistantDevice()->getRange();
  // sumDist[i] = recDist[i][index[i]];

  // Use the last sum and add the current value and substract latest value
  // sumDist[i] = sumDist[(index[i] + windowSize - 1) % windowSize] + recDist[i][index[i]] - recDist[i][(index[i] + 1 ) % windowSize];
  // sumDist[i] = sumDist[(index[i] - 1 + windowSize) % windowSize] + recDist[i][index[i]] - recDist[i][(index[i] + 1 ) % windowSize];
  float sum;
  for (int j = 0; j < windowSize; j ++) {
    sum += recDist[i][j] ;
  }
  sumDist[i] = sum;
  // latestDist[i] = (index[i] - 1 + windowSize) % windowSize;
  index[i] = (index[i] + 1 ) % windowSize;
  /*byte adr[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    adr = DW1000Ranging.getDistantDevice()->getByteAddress();
    Serial.print("from: ");
    for (int i = 0; i < 8; i++) {
    Serial.print(adr[i], HEX);
        Serial.print(" ");
    }*/
  // Serial.print("Device: "); Serial.println(i);
  //Serial.print("\t Range: "); Serial.print(latestDist[i]); Serial.print(" m");
  // Serial.print("\t Range: "); Serial.print(DW1000Ranging.getDistantDevice()->getRange()); Serial.print(" m");
  // Serial.print("\t RX power: "); Serial.print(DW1000Ranging.getDistantDevice()->getRXPower()); Serial.println(" dBm");

}


