#include "Trilateration.h"
#include "math.h"
#include "file.h"

void trila_2D(struct Data data, float result[3]){
/*  Compute the trilateration algorithm for a 2D search
Return the value of x and y of the target. We use the COLA algorithm to compute them
the anchor coordinates and the distance between the anchor and the target are store in data
and the result are store in the matrix result */

    float alpha, beta, num_x,num_y,den;
    alpha = (sq(data.dis_1)-sq(data.dis_2))-(sq(data.pos_1[0])-sq(data.pos_2[0]))-(sq(data.pos_1[1])-sq(data.pos_2[1]));
    beta = (sq(data.dis_1)-sq(data.dis_3))-(sq(data.pos_1[0])-sq(data.pos_3[0]))-(sq(data.pos_1[1])-sq(data.pos_3[1]));
    num_x = determinant_2(alpha,2*(data.pos_2[1]-data.pos_1[1]),beta,2*(data.pos_3[1]-data.pos_1[1]));
    num_y = determinant_2(2*(data.pos_2[0]-data.pos_1[0]),alpha,2*(data.pos_3[0]-data.pos_1[0]),beta);
    den = determinant_2(2*(data.pos_2[0]-data.pos_1[0]),2*(data.pos_2[1]-data.pos_1[1]),2*(data.pos_3[0]-data.pos_1[0]),2*(data.pos_3[1]-data.pos_1[1]));
    result[0] = num_x / den;
    result[1] = num_y / den;
    result[2] = 0; //No coordinate for z axis

/* Test data
    testData.pos_1[0] = 300;
    testData.pos_1[1] = 10;
    testData.pos_2[0] = 50;
    testData.pos_2[1] = 200;
    testData.pos_3[0] = 0;
    testData.pos_3[1] = 0;
    testData.dis_1 = sqrt(122900);
    testData.dis_2 = sqrt(32808);
    testData.dis_3 = sqrt(141559);
*/
};

void trila_3D(struct Data data, float result[3]){
/*  Compute the trilateration algorithm for a 2D search
Return the value of x and y of the target. We use the COLA algorithm to compute them
the anchor coordinates and the distance between the anchor and the target are store in data
and the result are store in the matrix result */

    float alpha, beta,gamma, num_x,num_y,num_z,den;
    alpha = (sq(data.dis_1)-sq(data.dis_2))-(sq(data.pos_1[0])-sq(data.pos_2[0]))-(sq(data.pos_1[1])-sq(data.pos_2[1]))-(sq(data.pos_1[2])-sq(data.pos_2[2]));
    beta = (sq(data.dis_1)-sq(data.dis_3))-(sq(data.pos_1[0])-sq(data.pos_3[0]))-(sq(data.pos_1[1])-sq(data.pos_3[1]))-(sq(data.pos_1[2])-sq(data.pos_3[2]));
    gamma = (sq(data.dis_1)-sq(data.dis_4))-(sq(data.pos_1[0])-sq(data.pos_4[0]))-(sq(data.pos_1[1])-sq(data.pos_4[1]))-(sq(data.pos_1[2])-sq(data.pos_4[2]));
    num_x = determinant_3(alpha,2*(data.pos_2[1]-data.pos_1[1]),2*(data.pos_2[2]-data.pos_1[2]),
                          beta,2*(data.pos_3[1]-data.pos_1[1]),2*(data.pos_3[2]-data.pos_1[2]),
                          gamma,2*(data.pos_4[1]-data.pos_1[1]),2*(data.pos_4[2]-data.pos_1[2]));

    num_y = determinant_3(2*(data.pos_2[0]-data.pos_1[0]),alpha,2*(data.pos_2[2]-data.pos_1[2]),
                          2*(data.pos_3[0]-data.pos_1[0]),beta,2*(data.pos_3[2]-data.pos_1[2]),
                          2*(data.pos_4[0]-data.pos_1[0]),gamma,2*(data.pos_4[2]-data.pos_1[2]));

    num_z = determinant_3(2*(data.pos_2[0]-data.pos_1[0]),2*(data.pos_2[1]-data.pos_1[1]),alpha,
                          2*(data.pos_3[0]-data.pos_1[0]),2*(data.pos_3[1]-data.pos_1[1]),beta,
                          2*(data.pos_4[0]-data.pos_1[0]),2*(data.pos_4[1]-data.pos_1[1]),gamma);

    den = determinant_3(2*(data.pos_2[0]-data.pos_1[0]),2*(data.pos_2[1]-data.pos_1[1]),2*(data.pos_2[2]-data.pos_1[2]),
                        2*(data.pos_3[0]-data.pos_1[0]),2*(data.pos_3[1]-data.pos_1[1]),2*(data.pos_3[2]-data.pos_1[2]),
                        2*(data.pos_4[0]-data.pos_1[0]),2*(data.pos_4[1]-data.pos_1[1]),2*(data.pos_4[2]-data.pos_1[2]));

    result[0] = num_x / den;
    result[1] = num_y / den;
    result[2] = num_z / den;
};

