// Copyright 2016, University of Freiburg,


// Register name and adress.
#define DEV_ID 0x00
#define EUI 0x01
#define PANADR 0x03
#define SYS_CFG 0x04
#define SYS_TIME 0x06
#define TX_FCTRL 0x08
#define TX_BUFFER 0x09
#define DX_TIME 0x0A
#define RX_FWTO 0x0C
#define SYS_CTRL 0x0D
#define SYS_MASK 0x0E
#define SYS_STATUS 0x0F
#define RX_FINFO 0x10
#define RX_BUFFER 0x11
#define RX_FQUAL 0x12
#define RX_TTCKI 0x13
#define RX_TTCKO 0x14
#define RX_TIME 0x15
#define TX_TIME 0x17
#define TX_ANTD 0x18
#define SYS_STATE 0x19
#define ACK_RESP_T 0x1A
#define RX_SNIFF 0x1D
#define TX_POWER 0x1E
#define CHAN_CTRL 0x1F
#define USR_SFD 0x21
#define AGC_CTRL 0x23
#define EXT_SYNC 0x24
#define ACC_MEM 0x25
#define GPIO_CTRL 0x26
#define DRX_CONF 0x27
#define RF_CONF0x28
#define TX_CAL 0x2A
#define FS_CTRL 0x2B
#define AON 0x2C
#define OTP_IF 0x2D
#define LDE_CTRL 0x2E
#define DIG_DIAG 0x2F
#define PMSC 0x36

/*
 * Write or read at the given register adress.
 * @param readWrite
 *          = 0 for reading operation; = 1 for writing operation.
 * @param regAddr 
 *          Adress of the register.
 * @param octets 
 *          number of octets which should be written/read.
 * @param data 
 *          buffer of data to be written/read. 
 * @param sub 
 *          0 : no sub address necessary
 *          1 : we are using sub address
 *          2 : extended address necessary
 * @param extAddr 
 *          extended Adress 4 bytes: SubSubExtExt (example: 0xffdd) if there is no extended adress the value should be zero (example: 0xff00).
 */
void readWriteToRegister(boolean readWrite, byte regAddr, unsigned short octets, byte* data, unsigned short sub, unsigned short extAddr = 0 );

/*
 * Apply the initial configuration which is described in the user manual..
 */
void initConfig();

/*
 *  Number of the pin to which the chip select line is connected.
 */
extern int chipSelect;
