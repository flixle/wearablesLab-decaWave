#ifndef FILE_H
#define FILE_H
#include "Trilateration.h"
#include <windows.h>
using namespace std;

void save_data(ofstream& doc, struct Data data, float result[3]);
/* Save the date in a text file in the following way :
x_target y_target z_target anchor_number x_anchor y_anchor distance_from_anchor */

struct Data import_data(string name, int line);
/* Import the data from the file " name ". We are able to choose the line we want to compute using the line argument.
Return a struct Data containing the values of x,y and z and the distance from each anchor.
The first line of the file have to be the coordinates of the anchors x,y,z and the other will be the distance from the
anchor to the target in the same order than the coordinates*/

HANDLE init_port(LPCSTR port);
/* Open the COM port for the communication */

void close_com(HANDLE m_hComPort);
/* Close a COM port */

bool read(struct Data * data,HANDLE m_hComPort);
/* write on the strut Data the data send on the port m_hComPort return true if successful false either
 */

bool send_mess(HANDLE m_hComPort);
/* Send a wake up message on the COM port m_hComPort */


#endif // FILE_H
