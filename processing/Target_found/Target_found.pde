//DO TO : translate distances to pixels !!!

int num_base=3; // Number of bases 3 for 2D 4 for 3D

float[][] coor_base = new float[4][3]; // One ligne per value and one colon per axis 
int SR = 10; //diameter of the ellipse (bases)
float minix,miniy,miniz,maxix,maxiy,maxiz; //minimum and maximum for x,y and z
int L = 1000;// size of the window (length and width)
int l = 600;
float rot_x = PI/2; //Rotation on x
float rot_y = 0;// Rotation on y
float rot_z = 0; // Rotation on z
float delta_x=0; //offset on x
float delta_y=0; //offset on y
float delta_z=0; //offset on z
float increment = PI/20; // increment for the rotation
float scale = 0.08; // Scaling factor, if >0 reduce the size 
float[] distance = new float[4]; // distance from the anchor to the nodes
float[] coor_target = new float[3]; // coordonates of the target
float[] data; // Variables for the imported data
boolean draw_mode=false; // Draw mode:if true only print the anchor and the bases othervise 
//also print the spheres or circles representing the distances

PFont f; // Front

void settings()
  {
    // import data from file 
    String[] aux = loadStrings("Test_save.txt");
  // Convert string into an array of integers using ' ' as delimiter
    data = float(split(aux[0],' '));
    
    // Initialize the bases coordinates
    coor_base[0][0] = data[4];
    coor_base[0][1] = data[5];
    coor_base[0][2] = data[6];
    coor_base[1][0] = data[9];
    coor_base[1][1] = data[10];
    coor_base[1][2] = data[11];
    coor_base[2][0] = data[14];
    coor_base[2][1] = data[15];
    coor_base[2][2] = data[16];
    coor_base[3][0] = data[19];
    coor_base[3][1] = data[20];
    coor_base[3][2] = data[21];
    
    // Initialize the distances
    distance[0] = data[7];
    distance[1] = data[12];
    distance[2] = data[17];
    distance[3] = data[22];
    
    // Initialize the target coordinates
    coor_target[0] = data[0];
    coor_target[1] = data[1];
    coor_target[2] = data[2];
    resize();
    refresh();
    if (num_base == 3)
      size(L,l,P3D);
    else 
      size(L, l, P3D);
    delta_z = -(2*height/3*scale); //center z=0 in the middle og the window 
    // For z=0 in the buttom of the screen: -(height*scale); on the top: delta_z=0
  }

void setup()
  {
    background(250); //Set the backgroud to hellgray
    f = createFont("Arial",16); 
    hint(ENABLE_DEPTH_TEST); // Allow the sphere to be transparent
    hint(ENABLE_DEPTH_SORT); // Same    
  }
 ///------------------------------------------Draw------------------------///
void draw() //<>//
{
  frameRate(4); // Refresh frequency: Number of frame per seconds
  refresh();
  //Choose between 2D and 3D and draw mode
  if (num_base == 3){
    if (draw_mode)
      plot_target_2D();
     else{
      background(250); 
      plot_2D();
    }
  }
  else if (num_base == 4){
    if (draw_mode){
      place_target_3D();
      place_wall_3D();
    }
    else{
    background(250); 
    plot_3D();
    }   
  } //<>//
  else {print("Error: Incorrect number of bases");
  }
}

///-----------------------Resize--------------------------------///
void resize() //Resize the window by taking in account the bases coordinates
{
  float[] values = {coor_base[0][0],coor_base[1][0],coor_base[2][0],coor_base[3][0]};
  minix=min(values);
  maxix=max(values);
  float[] values1 = {coor_base[0][1],coor_base[1][1],coor_base[2][1],coor_base[3][1]};
  miniy=min(values1);
  maxiy=max(values1);
  float[] values2 = {coor_base[0][2],coor_base[1][2],coor_base[2][2],coor_base[3][2]};
  maxiz=max(values2);
  miniz=min(values2);
  
  if (minix<0)
    delta_x += - minix;
  if (miniy<0)
    delta_y += - miniy;
  if (num_base == 3){
    L=int(maxix+delta_x+200);
    l=int(maxiy+delta_y+200);
  };
  L = 1200;
  l = 600; 
   //Ajust the scale factor
  //if (((L/(maxix - minix)) < 1) || ((l/(maxiy - miniy) < 1))){
    if (draw_mode){
       scale = min ((L/(maxix - min(0,minix))),(l/(maxiy - min(0,miniy))));
       print(scale);
       scale = 1/scale;
       print("  ");
      /* if (scale > 1)
         scale = int (scale)+1;
       else 
         scale = 1;
       scale = max(0.21,1/scale);*/
       print(scale);}
       scale = 1;
      //};
} 

///-----------------------keyPressed(Rotation manager)--------------------------------///
void keyPressed(){ //Rotate the 3D view with the keyboard
  if (keyCode == UP){ // If UP pressed rotated not clockwise around x 
    rot_x -= increment;
  }
  else if (keyCode == DOWN){// rotated clockwise around x
    rot_x += increment;
  }
  else if (keyCode == LEFT){ //clockwise rotation around y
    rot_y += increment;
  }
  else if (keyCode == RIGHT){ //not clockwise rotation around y
    rot_y -=increment;}
  else if (keyCode == ALT){ //clockwise around z
    rot_z += increment;
  }
  else if (keyCode == CONTROL){ //not clockwise around z
    rot_z -=increment;
  }
  else if (key == BACKSPACE){
    background(250);
  }
}

///----------------------- Refresh --------------------------------///
void refresh() // refresh the plotting screen by loading new values and take in account the offset and scale 
{
   //background(250,250,250);
   String[] aux = loadStrings("Test_save.txt"); //Load the f=data from the file
   //Avoid OutOfRAnge Expection: We measure the size of the array to make it's not empty
   int arraySize = 0;
   for (int i=0 ; i< aux.length; i++)
      arraySize++;
   if(arraySize == 0){
     return; // If it'e empty the exit the function
   }
    // Convert string into an array of integers using ' ' as a delimiter 
    data = float(splitTokens(aux[0])); 
     // Coordinate of the bases
      coor_base[0][0] = (data[4]+delta_x)/scale;
      coor_base[0][1] = (data[5]+delta_y)/scale;
      coor_base[0][2] = (data[6]+delta_z)/scale;
      coor_base[1][0] = (data[9]+delta_x)/scale;
      coor_base[1][1] = (data[10]+delta_y)/scale;
      coor_base[1][2] = (data[11]+delta_z)/scale;
      coor_base[2][0] = (data[14]+delta_x)/scale;
      coor_base[2][1] = (data[15]+delta_y)/scale;
      coor_base[2][2] = (data[16]+delta_z)/scale;
      coor_base[3][0] = (data[19]+delta_x)/scale;
      coor_base[3][1] = (data[20]+delta_y)/scale;
      coor_base[3][2] = (data[21]+delta_z)/scale;
      distance[0] = data[7]/scale;
      distance[1] = data[12]/scale;
      distance[2] = data[17]/scale;
      distance[3] = data[22]/scale;
      coor_target[0] = (data[0]+delta_x)/scale;
      coor_target[1] = (data[1]+delta_y)/scale;
      coor_target[2] = (data[2]+delta_z)/scale;
}

///----------------------- 2D --------------------------------///
void place_bases_2D() //Place the bases in the windows
{
  fill(51,153,255);//blue
  noStroke();
  ellipse(coor_base[0][0],coor_base[0][1],SR,SR);//base 1
  ellipse(coor_base[1][0],coor_base[1][1],SR,SR);//base 2
  ellipse(coor_base[2][0],coor_base[2][1],SR,SR);//base 3
}

void distance_circle_2D()//Draw the circles
{
  noFill();
  stroke(180,180,180);//grey
  //Distances (2*distance to get the diameter)
  ellipse(coor_base[0][0],coor_base[0][1],distance[0]*2,distance[0]*2);
  ellipse(coor_base[1][0],coor_base[1][1],distance[1]*2,distance[1]*2);
  ellipse(coor_base[2][0],coor_base[2][1],distance[2]*2,distance[2]*2);
  smooth();
}

void plot_distance_2D() // Plot the length of the radius (distance from anchor to target)
{
  stroke(128,128,250);
  line(coor_base[0][0],coor_base[0][1],coor_target[0],coor_target[1]);
  line(coor_base[1][0],coor_base[1][1],coor_target[0],coor_target[1]);
  line(coor_base[2][0],coor_base[2][1],coor_target[0],coor_target[1]);
    //Text
  textFont(f);
  fill(0);
  textAlign(CENTER);
  fill(255,100,100);
  text(distance[0],(coor_base[0][0]+coor_target[0])/2,(coor_base[0][1]+coor_target[1])/2);
  text(distance[1],(coor_base[1][0]+coor_target[0])/2,(coor_base[1][1]+coor_target[1])/2);
  text(distance[2],(coor_base[2][0]+coor_target[0])/2,(coor_base[2][1]+coor_target[1])/2);  
}

void plot_target_2D() //Plot the target
{
  fill(250,0,0);//red
  noStroke();
  ellipse(coor_target[0],coor_target[1],SR,SR);//Target
  smooth();
}

void plot_2D() // Cares about all the 2D plotting
{
  place_bases_2D();
  distance_circle_2D();
  plot_distance_2D();
  plot_target_2D();
}

///----------------------- 3D --------------------------------///
void plot_3D(){ // Cares about all the 3D plotting
  place_target_3D();
  place_bases_3D();
  place_wall_3D();
}

void place_bases_3D(){
  //Each anchor has to be handle in a different matrix to make them independant from each other so 
  // the rotation has to be done in each matrix
  
  lights();
  // plot of the first anchor and of a sphere representing the distance to the target
  pushMatrix();
  // Rotation if needed
  rotateY(rot_y); //rotate around Y
  rotateX(rot_x); //rotate around X
  rotateZ(rot_z); //rotate around Z
  // Move the pointed to coordinates of the anchor
  translate(coor_base[0][0],coor_base[0][1],coor_base[0][2]); 
  noStroke();
  fill(200,0,0,20);
  stroke(200,0,0,50);
  sphere(distance[0]);//Draw a sphere with a radius equal to the distance from the target to the anchor
  sphere(SR/2);// draw a point for the anchor
  popMatrix();
  
  pushMatrix();
  //noFill();
  rotateY(rot_y);
  rotateX(rot_x);
  rotateZ(rot_z);
  noStroke();
  translate(coor_base[1][0],coor_base[1][1],coor_base[1][2]);
  fill(0,255,0,20);//green
  stroke(0,255,0,50);
  sphere(distance[1]);
  sphere(SR/2); //
  popMatrix(); //<>//
  
  pushMatrix();
  noStroke();
  rotateY(rot_y);
  rotateX(rot_x);
  rotateZ(rot_z);
  translate(coor_base[2][0],coor_base[2][1],coor_base[2][2]);
  fill(0,0,250,20);//blue
  stroke(0,0,250,50);
  sphere(distance[2]);
  sphere(SR/2);
  popMatrix(); //<>//
  
  pushMatrix();
  rotateY(rot_y);
  rotateX(rot_x);
  rotateZ(rot_z);
  translate(coor_base[3][0],coor_base[3][1],coor_base[3][2]);
  stroke(150,150,150,20); 
  fill(150,150,150,50);
  sphere(distance[3]);
  sphere(SR/2);
  popMatrix(); //<>//
}

void place_target_3D(){// plot the target
  pushMatrix();
  rotateY(rot_y);
  rotateX(rot_x);
  rotateZ(rot_z);
  translate(coor_target[0],coor_target[1],coor_target[2]);
  noStroke();
  fill(0,0,0,255);
  sphere(SR);
  popMatrix();
}

void place_wall_3D(){ ///Place the ground at (0,0,0)
  pushMatrix();
  rotateX(rot_x);
  rotateY(rot_y);
  rotateZ(rot_z);
  fill(100,100,100,20);
  translate(0, 0, delta_z/scale);
  rect(0,0,600,600);
  popMatrix();
}