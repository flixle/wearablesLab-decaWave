#ifndef TRILATERATION_H
#define TRILATERATION_H

/* Data structure for the trilateration algorithm*/

    struct Data{
        float pos_1[3]; // position of the first anchor [x,y]
        float pos_2[3]; //idem for the second anchor
        float pos_3[3]; // and the third one
        float pos_4[3]; // position of the forth one : only use for 3D
        float dis_1; // distance of the target from anchor 1
        float dis_2; // distance of the target from anchor 2
        float dis_3; // distance of the target from anchor 3
        float dis_4; // distance of the target from anchor 4 use for 3D only
        };

void trila_2D(struct Data data,float result[3]);
/* Compute 2D trilateration and write the result in result
input: struct Data  containing all the distance and anchor coordinates
       pointer on result which is update with the coordinates of the target
*/

void trila_3D(struct Data data, float result[3]);
/* Compute 3D trilateration and and write the result in result
input: struct Data  containing all the distances and anchor coordinates
       pointer on result which is update with the coordinates of the target
*/


#endif // TRILATERATION_H
