#include <iostream>
#include "math.h"
#include "Trilateration.h"
#include <fstream>
#include <stdlib.h> // For sleep
#include "file.h"
#include <windows.h>
#include <conio.h>

#define sleepTime 100

using namespace std;

/* Data format  : time, basic number */
/* TODO: find the position of the third anchor according to the distance between
3 and 1 and 3 and 2. By assuming that x_1, y_1 = 0 and y_2 = 0
*/

int main()
{
    // Variables
    bool test = true; // reading or sending successful if true
    bool testing; // Control variables
    bool display = false; //Target coordinates print or not (not if false)
    //Data:
    struct Data data; //Structure of the input data
    float result[3]; // Array of the target coordinates in the same unit than the one use form the inputs
    // File Saving: path to the text file name Test_save.txt used by processing
    string name = "C:/Users/Co/Documents/INSA/4 AE/S2 Freiburg/wearablesLab-decaWave/Processing/Target_found/Test_save.txt"; // Name of the saving file
    // Ports
    HANDLE m_hComPort; // Port creation
    int number_line = 0; // Number of lines to be compute in the testfile
    int input = 0; // Switch case
    TCHAR *pcCommPort = TEXT("\\\\.\\COM27");// Port Name for Bluetooth communication

    // Intermediate variables :
    string plot;
    int index = 0;
    int in = 0;

   //Import data from Testing_data and return a struct Data
    cout << "\n Hello ! \n" << endl;
    cout << " Welcome to our homemade localization interface \n" << endl;
    cout << " Don't forget to use the Processing interface to visualize your results! " << endl;
    cout << " Have fun! \n" << endl;
    cout << " You can leave the software by pressing any key once the computation running \n" <<endl;
    cout << " Infos about the graphic interface: If you press BACKSPACE the graphic interface will reset" << endl;
    cout << " Use the arrows to rotate: LEFT and RIGHT to turn around the Y axis UP and DOWN for the X one and CTRL and ALT for the Z axis \n" << endl;

    //init_ref(&testData);

    /*
    cout << testData.pos_1[0] << " " << testData.pos_1[1] << " " << testData.pos_1[2] << endl;
    cout << testData.pos_2[0] << " " << testData.pos_2[1] << " " << testData.pos_2[2] << endl;
    cout << testData.pos_3[0] << " " << testData.pos_3[1] << " " << testData.pos_3[2] << endl;
    cout << testData.pos_4[0] << " " << testData.pos_4[1] << " " << testData.pos_4[2] << endl;
    */


    // User choose which function he want to use
    cout << "1. 3D & Import data from file " << endl;
    cout << "2. 3D & Use Bluetooth connexion" << endl;
    cout << "3. 2D & Import data from file " << endl;
    cout << "4. 2D & Use Bluetooth connexion " << endl;
    cin >> input ;

    cout << "Do you want to display the target coordinates? (y/n)";
    cin >> plot;

    if (plot == "y")
        display = true;

    switch ( input ) {
    case 1:
        cout << "\n How many data line do you want to compute from testfile ? ";
        cin >> number_line;
        while(!_kbhit()){
            //For 3D : "Test_3D.txt","Test_3D_self_coordinates.txt"
            data = import_data("Test_3D_self_coordinates.txt", index);
            // Compute the position of the target
            trila_3D(data, result);
            if (display)
                cout << result[0] << " " << result[1] << " "<< result[2] << endl;

            // Open saving file
            ofstream doc(name.c_str());
            if(!doc)
            {
                cout << "Error: file cannot be open for saving" << endl;
            }
            save_data(doc, data, result); // Save the computed data in a file doc
            index ++;
            if (index == number_line + 1)
                index = 1;
            _sleep(sleepTime); //Times in ms
        }
        break;

    case 2:
        // Init
            // For port greater than 9 need to use \\\\.\\ as beginning of the name

        m_hComPort = CreateFile( pcCommPort,
                      GENERIC_READ | GENERIC_WRITE,
                      0,      //  must be opened with exclusive-access
                      NULL,   //  default security attributes
                      OPEN_EXISTING, //  must use OPEN_EXISTING
                      0,      //  not overlapped I/O
                      NULL ); //  hTemplate must be NULL for comm devices

   if (m_hComPort == INVALID_HANDLE_VALUE)
   {
       //  Handle the error.
       printf ("CreateFile failed with error %d.\n", GetLastError());
       return (1);
   }
    //
        init_ref(&data);

        while(!_kbhit()){
            testing = read(&data, m_hComPort);
            if (!testing){
                cout << "error reading data from Bluetooth modules" <<endl;
            }
            // Compute the position of the target
            trila_3D(data, result);
            if (display)
                cout << result[0] <<" "<< result[1] <<" "<<result[2]<<endl;
           ofstream doc(name.c_str());
            if(!doc)
            {
                cout << "Error: file cannot be open for saving" << endl;
            }
            save_data(doc, data, result); // Save the computed data in a file doc
            _sleep(sleepTime); //Times in ms
        }
        break;

    case 3:
        cout << "How many data line do you want to compute from testfile ? ";
        cin >> number_line;
        while(!_kbhit()){
            //For 2D :"Testing_data_same_anchor.txt"
            data = import_data("Testing_data_same_anchor.txt", index);
            // Compute the position of the target
            trila_2D(data, result);
            if (display)
                cout << result[0] <<" "<< result[1] <<" "<<result[2]<<endl;
            ofstream doc(name.c_str());
            if(!doc)
            {
                cout << "Error: file cannot be open for saving" << endl;
            }
            save_data(doc, data, result); // Save the computed data in a file doc
            index ++;
            if (index == number_line + 1)
                index = 1;
            _sleep(sleepTime); //Times in ms
        }
        break;

    case 4:
         // Init
            // For port greater than 9 need to use \\\\.\\ as beginning of the name

        m_hComPort = CreateFile( pcCommPort,
                      GENERIC_READ | GENERIC_WRITE,
                      0,      //  must be opened with exclusive-access
                      NULL,   //  default security attributes
                      OPEN_EXISTING, //  must use OPEN_EXISTING
                      0,      //  not overlapped I/O
                      NULL ); //  hTemplate must be NULL for comm devices

   if (m_hComPort == INVALID_HANDLE_VALUE)
   {
       //  Handle the error.
       printf ("CreateFile failed with error %d.\n", GetLastError());
       return (1);
   }
    //
        init_ref(&data);
        while(!_kbhit()){
            testing = read(&data, m_hComPort);
            if (!testing){
                cout << "error reading data from Bluetooth modules" <<endl;
            }
            // Compute the position of the target
            trila_2D(data, result);
            if (display)
                cout << data.dis_1 <<" " << data.dis_2 <<" "<< data.dis_3 <<endl;
                cout <<"d " << result[0] <<" "<< result[1] <<" "<<result[2]<<endl;
            ofstream doc(name.c_str());
            if(!doc)
            {
                cout << "Error: file cannot be open for saving" << endl;
            }
            save_data(doc, data, result); // Save the computed data in a file doc
            doc.close();
            _sleep(sleepTime); //Times in ms
            test = PurgeComm(m_hComPort, PURGE_RXCLEAR |PURGE_RXABORT |PURGE_TXABORT | PURGE_TXCLEAR);
        }
        break;

    default:
        cout << " Error, bad input, only 1,2,3 and 4 was accepted !" << endl;
        cout << " The program is now going to stop. "<< endl;
        cout << endl;
        cout << " Bye! "<< endl;
        _sleep(1000); // Stop after 1 second

        break;
    }

    /*while (1)
    {
        //For 3D : "Test_3D.txt","Test_3D_self_coordinates.txt"
        //For 2D :"Testing_data_same_anchor.txt"
        testData = import_data("Test_3D_self_coordinates.txt",i);
        // Compute the position of the target
        trila_3D(testData,result);

        //Print in the console
        //cout << testData.dis_1<<" x = "<< result[0] <<" y =  "<< result[1] << " and z = " <<result[2]<<endl;
        //cout << result[0] <<" "<< result[1] <<" "<<result[2]<<endl;

        // Saving in a txt file
        string name = "C:/Users/Co/Documents/INSA/4 AE/S2 Freiburg/wearablesLab-decaWave/Processing/Target_found/Test_save.txt"; // Name of the saving file
            //open the saving file
        ofstream doc(name.c_str());
        if(!doc)
            {
                cout << "Error: file cannot be open for saving" << endl;
            }
        else
        {
            save_data(doc,testData,result); // Save the computed data in a file doc
        }
       _sleep(500); //Times in ms
        i++;
        if (i == 2)
            {
                i=1;
            }
        tet ++;
    };
*/
/*
        //Test of the Bluetooth
        //m_hComPort = init_port(port);
        int i=0;

    char input;
    string distance;
    DWORD dwReading;
    int dwSize = 1; // number of read bytes (8bits)
    char* pEnd;
    //HANDLE m_hComPort;
    TCHAR *pcCommPort = TEXT("\\\\.\\COM23");

   //  Open a handle to the specified com port.
   m_hComPort = CreateFile( pcCommPort,
                      GENERIC_READ | GENERIC_WRITE,
                      0,      //  must be opened with exclusive-access
                      NULL,   //  default security attributes
                      OPEN_EXISTING, //  must use OPEN_EXISTING
                      0,      //  not overlapped I/O
                      NULL ); //  hTemplate must be NULL for comm devices

   if (m_hComPort == INVALID_HANDLE_VALUE)
   {
       //  Handle the error.
       printf ("CreateFile failed with error %d.\n", GetLastError());
       return (1);
   }
    // Read the input of the bluetooth module character by character and concatenate them in a string
        ReadFile(m_hComPort, &input, dwSize, &dwReading, NULL);
        //distance = distance + input;
        //cout<<input;

*/
/*
        // Bluetooth testing 2:
  //  Open a handle to the specified com port.
   // TCHAR *pcCommPort = TEXT("\\\\.\\COM20"); // For port greater than 9 need to use \\\\.\\ as beginning of the name

   m_hComPort = CreateFile( pcCommPort,
                      GENERIC_READ | GENERIC_WRITE,
                      0,      //  must be opened with exclusive-access
                      NULL,   //  default security attributes
                      OPEN_EXISTING, //  must use OPEN_EXISTING
                      0,      //  not overlapped I/O
                      NULL ); //  hTemplate must be NULL for comm devices

   if (m_hComPort == INVALID_HANDLE_VALUE)
   {
       //  Handle the error.
       printf ("CreateFile failed with error %d.\n", GetLastError());
       return (1);
   }

    while (1){
            testing = read(&data, m_hComPort);
            //testing = send_mess(m_hComPort);
            cout << "d " << data.dis_1 << "  " << data.dis_2 <<"  "<<data.dis_3 <<"  "<<data.dis_4 << endl;
            in ++;
            test = PurgeComm(m_hComPort, PURGE_RXCLEAR |PURGE_RXABORT |PURGE_TXABORT | PURGE_TXCLEAR);
        };
    close_com(m_hComPort);
*/

    if (input == 2 || input == 4)
    cout << "Stop" << endl;
        close_com(m_hComPort);
    cout << " \n Hope you enjoyed our software \n" << endl;
    cout << "  Bye \n" <<endl;
    cout << "                                      The UWBCap team"<<endl;
    _sleep(5000);

    return 0;
}

