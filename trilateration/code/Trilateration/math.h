#ifndef MATH_H
#define MATH_H
#include <fstream>
#include <iostream>
#include <cstdlib>

float sq (float value);
/* Return the square of the value
Example sq(3) returns 9*/

float determinant_2(float a1,float a2,float b1,float b2);
/*Compute the determinant of a 2x2 matrix [a1 b1; a2 b2]
Example : determinant_2(4,6,4,2) gives -16 */


float determinant_3(float a,float b,float c,float d,float e,float f,float g,float h,float i);
/*Compute the determinant of a 3x3 matrix [a b c; d e f; g h i]
Example determinant_3(4,6,3,4,2,3,5,4,6) gives -36 */

void init_ref(struct Data * data);
/* Define a relative mark only knowing the relative distances from one anchor to an other
The function modified the struct Data given in argument
Input from the software: pointer on a data
Input of the environment: User must enter the distance when the interface ask him to.
Output: none
*/
#endif // MATH_H
